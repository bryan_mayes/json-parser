import fileinput

class JsonStringStreamer():

    the_file_iterator = None
    line_buffer = ""
    char_pointer = 0
    position = 0;

    def __init__(self, buffer=None):
        if buffer == None:
            self.the_file_iterator = fileinput.input()
        else:
            if not isinstance(buffer, basestring):
                raise ValueError

            self.the_file_iterator = iter(buffer)

    # eats spaces and line returns
    def get_next_char(self):

        try:
            if self.line_buffer:
                # we haven't run through the line yet.
                if self.char_pointer+1 < len(self.line_buffer):
                    return self.basic_increment_char()
                else:
                    self.char_pointer + 1
                    return self.reset_buffer()
            else:
                return self.reset_buffer()

        except StopIteration:
            raise StopIteration

        except:
            raise Exception("Something is wrong")

    # eats spaces and line returns
    def get_next_char_filtered(self):
        char = self.get_next_char()

        # keep getting chars until we've got a real character.
        if (char == " " or char == "\n"):
            char = self.get_next_char_filtered()
        return char.rstrip()

    def basic_increment_char(self):
        old = self.char_pointer
        self.char_pointer += 1
        return self.line_buffer[old]

    def reset_buffer(self):
        self.line_buffer = next(self.the_file_iterator)
        self.char_pointer = 0
        return self.basic_increment_char()