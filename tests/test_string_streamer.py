import unittest
from string_streamer import JsonStringStreamer

class TestFileReader(unittest.TestCase):

    def test_no_string_to_parse_causes_exception(self):
        with self.assertRaises(ValueError):
            JsonStringStreamer(1)


    def test_get_char_with_line_return(self):
        the_string = "hello\nroger"
        streamer = JsonStringStreamer(the_string)
        for char in range(0, len(the_string)-1):
            streamer.get_next_char()  # get us through most of the string

        # try to get the last char in the string.
        self.assertEquals(the_string[len(the_string)-1], streamer.get_next_char())


    def test_get_char_without_line_return(self):
        the_string = "hello"
        streamer = JsonStringStreamer(the_string)
        for char in range(0, len(the_string)-1):
            streamer.get_next_char()  # get us through most of the string

        # try to get the last char in the string.
        self.assertEquals(the_string[len(the_string)-1], streamer.get_next_char())

    def test_exceeding_string_length(self):
        the_string = "hello"
        streamer = JsonStringStreamer(the_string)
        for char in range(0, len(the_string)):
            streamer.get_next_char()  # get us through most of the string

        # try to get the last char in the string.
        with self.assertRaises(StopIteration):
            streamer.get_next_char()

    def test_simple_char_parse(self):
        the_string = "hello"
        streamer = JsonStringStreamer(the_string)
        self.assertEquals(the_string[0], streamer.get_next_char())