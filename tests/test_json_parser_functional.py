import unittest
import json  # yep we'll use this one to test :)
from json_parser import PurePythonJsonParser

class FunctionalTestsJsonParser(unittest.TestCase):

    def test_basic_dict(self):
        key = "key"
        value = "value"
        json_string = '{"' + key + '" : "' + value + '"}'
        parser = PurePythonJsonParser()
        native_obj = parser.parse(json_string)
        self.assertEquals({key : value}, native_obj)

    def test_basic_list(self):
        key = "key"
        value = "value"
        json_string = '["' + key + '" , "' + value + '"]'
        parser = PurePythonJsonParser()
        native_obj = parser.parse(json_string)
        self.assertEquals([key , value], native_obj)

    def test_nested_list(self):
        test_obj = ["first", ["items", "things"]]
        test_obj_str = json.dumps(test_obj)
        parser = PurePythonJsonParser()
        native_obj = parser.parse(test_obj_str)
        self.assertEquals(test_obj, native_obj)

    def test_nested_dict_in_list(self):
        test_obj = ["first", {"key1" : "value1", "key2" : "value2"}, "second"]
        test_obj_str = json.dumps(test_obj)
        parser = PurePythonJsonParser()
        native_obj = parser.parse(test_obj_str)
        self.assertEquals(test_obj, native_obj)

    def test_nested_list_in_dict(self):
        test_obj = {"key1" : ["first", "second"], "key2" : "value2"}
        test_obj_str = json.dumps(test_obj)
        parser = PurePythonJsonParser()
        native_obj = parser.parse(test_obj_str)
        self.assertEquals(test_obj, native_obj)

    def test_dict_with_escaped_strings(self):
        """ This feature needs to be implemented """

        test_obj = {"key1" : "this is the quote \"stuff \""}
        test_obj_str = json.dumps(test_obj)
        parser = PurePythonJsonParser()
        native_obj = parser.parse(test_obj_str)
        self.assertEquals(test_obj, native_obj)
