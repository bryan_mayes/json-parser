import mock
# from mock import MagicMock
import unittest
from json_parser import PurePythonJsonParser

class TestCreateLists(unittest.TestCase):
    def test_get_list_value(self):
        pass


class TestCreateDict(unittest.TestCase):

    # @mock.patch('string_streamer.JsonStringStreamer')
    # def TestBasic(self, JsonStringStreamer):
    #     item = JsonStringStreamer()
    #     parser = PurePythonJsonParser.create_dict(item)
    #     obj = parser.parse('{}')
    #     self.assertDictEqual(obj, {})
    pass


class TestBaseParse(unittest.TestCase):

    @mock.patch('json_parser.PurePythonJsonParser.create_dict', return_value={})
    def test_parse_empty_dict(self, create_dict):
        parser = PurePythonJsonParser()
        obj = parser.parse('{}')
        self.assertTrue(create_dict.called)
        self.assertDictEqual(obj, {})

    @mock.patch('json_parser.PurePythonJsonParser.create_list', return_value=[])
    def test_parse_empty_list(self, create_list):
        parser = PurePythonJsonParser()
        obj = parser.parse('[]')
        self.assertTrue(create_list.called)
        self.assertListEqual(obj, [])

    def test_cant_parse_anything_but_string(self):
        parser = PurePythonJsonParser()
        with self.assertRaises(ValueError):
            obj = parser.parse([])





