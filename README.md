# JSON Parser


## Usage
*  ./parse_json.py < list.json


## Known Issues
*  Doesn't parse numbers as values.
*  Doesn't parse escaped characters. ( i.e. { "key" : "val\"ue" } )
*  No global counter yet in the buffer class for reporting error positions.
*  Loops are limited and it should only be O(N) but some of the parsing functions might be optimized for better performance.

