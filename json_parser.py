from string_streamer import JsonStringStreamer


class PurePythonJsonParser():

    def __init__(self, json_string=None):
        if json_string:
            self.parse(json_string)

    def create_dict(self, buffer):
        the_dict = {}
        char = buffer.get_next_char_filtered()

        if char and char == '}':
            # special case with no keys or values.
            return the_dict

        elif char:
            # common case we expect to see keys and values
            char = self.get_key_value_pairs(buffer, char, the_dict)
            if not char == '}':
                # raise UnexpectedCharacter()
                raise UnexpectedCharacter("Expecting a '}' found: '" + char + "'")

        else:
            # that's a bummer no close
            raise DictNotClosed

        return the_dict

    def create_list(self, buffer):
        the_list = []
        char = buffer.get_next_char_filtered()
        if char and char == ']':
            # special case no items in the list.
            return the_list

        elif char:
            # first time through
            if len(the_list) == 0:
                self.get_list_value(buffer, char, the_list)
                char = buffer.get_next_char_filtered()

            while char == ',':
                char = buffer.get_next_char_filtered()
                self.get_list_value(buffer, char, the_list)
                char = buffer.get_next_char_filtered()

                if char == ']':
                    # we're done.
                    break

            # we need a close bracket now.  If its not there.. problems.
            if not char == ']':
                raise UnexpectedCharacter("Expecting ']' found: '" + char + "'")

        else:
            raise ListNotClosed

        return the_list

    def get_key_value_pairs(self, buffer, char, dictionary):
        key = self.get_key(buffer, char)
        value = self.get_dict_value(buffer)
        dictionary[key] = value

        char = buffer.get_next_char_filtered()
        if char == ",":
            char = buffer.get_next_char_filtered()
            char = self.get_key_value_pairs(buffer, char, dictionary)

        return char

    # in json values need to always be in quotes.
    def get_key(self, buffer, current_char):
        try:
            key_string = ''
            if current_char == '"':
                # OK we're good.  Let's read until the next un-escaped "
                char = buffer.get_next_char()
                while not char == '"':
                    key_string += char
                    char = buffer.get_next_char()
                return key_string

            else:
                raise UnexpectedCharacter("Expecting quote found: " + current_char)

        except StopIteration:
            raise KeyMissingEndQuote

        except Exception, e:
            raise e

    def get_dict_value(self, buffer):
        the_value = ''
        # search for : if not found then throw error
        char = buffer.get_next_char_filtered()
        if char == ":":
            char = buffer.get_next_char_filtered()
            if char == '"':
                try:
                    char = buffer.get_next_char_filtered()
                    while not char == '"':
                        the_value += char
                        char = buffer.get_next_char()
                    return the_value
                except StopIteration:
                    raise UnexpectedCharacter("Expecting a '\"' found: '" + char + "'")

            elif char == "{":
                return self.create_dict(buffer)
            elif char == "[":
                return self.create_list(buffer)
            else:
                raise UnexpectedCharacter("Expecting a '\"' found: '" + char + "'")
        else:
            raise UnexpectedCharacter("Expecting a ':' found: '" + char + "'")

    def get_list_value(self, buffer, char, the_list):
        """

        :param buffer:    This is the document we're parsing through.
        :param char:      The current character we're on.
        :param the_list:
        :return:
        """

        the_value = ''
        # search for : if not found then throw error
        if char == '"':
            try:
                char = buffer.get_next_char_filtered()
                while not char == '"':
                    the_value += char
                    char = buffer.get_next_char()
                the_list.append(the_value)
            except StopIteration:
                raise UnexpectedCharacter("Expecting a '\"' found: '" + char + "'")

        elif char == "{":
            the_list.append(self.create_dict(buffer))
        elif char == "[":
            the_list.append(self.create_list(buffer))
        else:
            raise UnexpectedCharacter("Expecting a '\"' found: '" + char + "'")

    def parse(self, json_string=None):
        """
        :param json_string: The string you want to parse or None if you want to fall back to reading
                            from the filesystem or STDIN
        :return: Native object or an Exception if the document is poorly formed JSON
        """
        native_obj = None
        buffer = JsonStringStreamer(json_string)
        try:
            char = buffer.get_next_char_filtered()

            if char and char == '{':
                native_obj = self.create_dict(buffer)
            elif char and char == '[':
                native_obj = self.create_list(buffer)
            else:
                raise Exception("")

            return native_obj

        except Exception, e:
            print e.message


class UnexpectedCharacter(Exception):
    pass


class KeyMissingEndQuote(Exception):
    message = "JSON string does not have an end quote"


class DictNotClosed(Exception):
    message = "dict not closed"


class ListNotClosed(Exception):
    message = "list not closed"
